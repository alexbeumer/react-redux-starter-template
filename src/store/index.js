import { applyMiddleware, compose, createStore, combineReducers } from "redux";
import { connectRouter, routerMiddleware } from "connected-react-router";
import thunk from "redux-thunk";
import { createBrowserHistory } from "history";
import { composeWithDevTools } from "redux-devtools-extension";
import * as reducers from "../reducers/index";

export const history = createBrowserHistory();

const composeEnhancers = composeWithDevTools({
    // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});
const initialState = {};
const rootReducer = combineReducers({ ...reducers });
const enhancers = [];
const middleware = [thunk, routerMiddleware(history)];
const store = createStore(
    connectRouter(history)(rootReducer), // new root reducer with router state
    initialState,
    composeEnhancers(applyMiddleware(...middleware), ...enhancers)
);

// Hot reloading
if (module.hot) {
    // Reload reducers
    module.hot.accept("../reducers", () => {
        store.replaceReducer(connectRouter(history)(rootReducer));
    });
}

export default store;
