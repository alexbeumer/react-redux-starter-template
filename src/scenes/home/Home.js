import React from "react";
import { push } from "connected-react-router";
import store from "../../store";

const handleArticles = () => {
    store.dispatch(push("/articles"));
};

export default () => (
    <div>
        <h1>Welcome</h1>
        <button onClick={handleArticles}>Articles</button>
    </div>
);
