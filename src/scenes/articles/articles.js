import React from "react";
import Form from "../../components/Form";
import List from "../../components/List";
import styles from "./Articles.css";

export default () => {
    return (
        <div>
            <div>
                <h1 className={styles.red}>Add a new article</h1>
                <Form />
            </div>
            <div>
                <h1>Articles</h1>
                <List />
            </div>
        </div>
    );
};
