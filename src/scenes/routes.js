import React from "react";
import { Route, Switch } from "react-router";
import Articles from "./articles/Articles";
import Home from "./home/Home";

export default (
    <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/articles" component={Articles} />
        <Route render={() => <div>Miss</div>} />
    </Switch>
);
