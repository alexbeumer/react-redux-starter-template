import React from "react";
import { ConnectedRouter } from "connected-react-router";
import { hot } from "react-hot-loader";
import PropTypes from "prop-types";
import styles from "./App.css";
import routes from "./scenes/routes";

const App = ({ history }) => (
    <div className={styles.app}>
        <ConnectedRouter history={history}>{routes}</ConnectedRouter>
    </div>
);

App.propTypes = {
    history: PropTypes.object
};

export default hot(module)(App);
